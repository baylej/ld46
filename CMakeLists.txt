cmake_minimum_required(VERSION 3.15)

project(ld46 VERSION 1.0.0 LANGUAGES C CXX)

if(CMAKE_SYSTEM_NAME EQUAL "Emscripten")
  set(EMSCRIPTEN True)
endif()

find_package(raylib REQUIRED)
# The following libs should be transitively imported by raylib...
find_package(Threads REQUIRED)
if(UNIX AND NOT EMSCRIPTEN)
  find_package(X11 REQUIRED)
endif()

# libTMX dependencies
include(FindLibXml2)
find_package(LibXml2 REQUIRED)
include(FindZLIB)
find_package(ZLIB REQUIRED)

add_executable(ld46
  src/main.cpp

  src/menu/menu.cpp
  src/gameover/gameover.cpp
  src/game/game.cpp

  src/game/map.cpp
  src/game/utils.cpp

  src/game/entities.cpp
  src/game/ent/leroy.cpp
  src/game/ent/foes.cpp
  src/game/ent/player.cpp

  src/game/tmx/tmx_xml.c
  src/game/tmx/tmx_err.c
  src/game/tmx/tmx_hash.c
  src/game/tmx/tmx_mem.c
  src/game/tmx/tmx.c
  src/game/tmx/tmx_utils.c)

if(EMSCRIPTEN)
  target_compile_definitions(ld46 PRIVATE EMSCRIPTEN)
endif()

target_link_libraries(ld46 PRIVATE raylib Threads::Threads LibXml2::LibXml2 ZLIB::ZLIB)
if(VCPKG_TOOLCHAIN)
  find_package(glm REQUIRED) # Set by vcpkg
  target_link_libraries(ld46 PRIVATE glm)
elseif(UNIX AND NOT EMSCRIPTEN)
  target_link_libraries(ld46 PRIVATE X11::X11)
elseif(EMSCRIPTEN)
  set_target_properties(ld46 PROPERTIES SUFFIX ".html")
  target_link_options(ld46 PRIVATE "SHELL:-s ASSERTIONS=1 -s FULL_ES3=1 -s USE_GLFW=3")
  # GLFW is provided by emscripten, GLFW3 must be activated otherwise GLFW2 is used
  target_link_options(ld46 PRIVATE --preload-file data --shell-file "${CMAKE_SOURCE_DIR}/shell.html")
  # embedded all data directory in
endif()

set_target_properties(ld46 PROPERTIES CXX_STANDARD 17 CXX_STANDARD_REQUIRED On)
target_compile_definitions(ld46 PRIVATE WANT_ZLIB)

if(NOT EXISTS "$CMAKE_BINARY_DIR/data") # not in-source builds
  file(COPY data/ DESTINATION data/ PATTERN src EXCLUDE)
endif()
