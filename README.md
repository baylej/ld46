# LudumDare 46

## Dependencies
* C++17 compiler (gcc, clang)
* CMake
* Raylib

## Build

```bash
mkdir build
cd build
cmake ..
make
./ld46
```
