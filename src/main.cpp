#include <cassert>
#include <iostream>
#include <raylib.h>

#include "context.h"
#include "menu/menu.h"
#include "game/game.h"
#include "gameover/gameover.h"

#ifdef EMSCRIPTEN
#include <emscripten/emscripten.h>
#endif

Context_holder CTX_HOLDER;

float time_holder = 0.f;

void update_and_draw()
{
	float curr_time = GetTime();
	if (IsKeyPressed(KEY_D)) {
		CTX_HOLDER.dbg_overlay = !CTX_HOLDER.dbg_overlay;
	}

	if (IsKeyPressed(KEY_F)) {
		CTX_HOLDER.fps_overlay = !CTX_HOLDER.fps_overlay;
	}

	CTX_HOLDER.get_context()->loop_run(curr_time - time_holder);

	if (CTX_HOLDER.fps_overlay) {
		BeginDrawing();
		DrawFPS(5, 5);
		EndDrawing();
	}
	time_holder = curr_time;
}

int main(int argc, char **argv)
{
	assert(DirectoryExists(CTX_HOLDER.data_dir.c_str()));
	InitWindow(CTX_HOLDER.display_width, CTX_HOLDER.display_height, CTX_HOLDER.game_name.c_str());
	if (!IsWindowReady()) {
		std::cerr << "Cannot create a window" << std::endl;
		return 1;
	}

	InitAudioDevice();
	//SetMasterVolume(.5f);

	// Create contexts in a scope
	{
		Menu menu;
		Game game;
		Gameover gameover;

		CTX_HOLDER.menu = &menu;
		CTX_HOLDER.game = &game;
		CTX_HOLDER.gameover = &gameover;

		game.init();

		CTX_HOLDER.set_context(CTX_HOLDER.menu);
		time_holder = GetTime();

#ifdef EMSCRIPTEN
		emscripten_set_main_loop(update_and_draw, 0, 1);
#else
		SetTargetFPS(CTX_HOLDER.fps);
		while (!WindowShouldClose()) {
			update_and_draw();
		}
#endif
	}

	CloseAudioDevice();
	CloseWindow();

	return 0;
}
