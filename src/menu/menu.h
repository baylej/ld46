#ifndef LD46_MENU_H
#define LD46_MENU_H

#include "../context.h"
#include "../game/game.h"

class Transition: public Context {
public:
	Transition() = default;
	void loop_run(float delta_t) override;
	void start() override;

private:
	Sprite baloon{"data/baloon.png"};
	float sleep = 0.f;
	Game* ctx;
};

class Menu: public Context {
public:
	Menu();
	void loop_run(float delta_t) override;

private:
	const int right_align;
	Transition transition;
};

#endif //LD46_MENU_H
