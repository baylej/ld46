#include <iostream>
#include <raylib.h>

#include "menu.h"

// A const constexpr to fix: warning: ISO C++ forbids converting a string constant to ‘char*’
constexpr const char* credits = "By @mrhidefr";

void Menu::loop_run(float delta_t)
{
	if (IsKeyPressed(KEY_ENTER)) {
		CTX_HOLDER.set_context(&transition);
	}

	// draw menu
	BeginDrawing();
	ClearBackground(BLACK);
	//ClearBackground(ColorFromHSV(Vector3{ 360 * (float)GetTime()/30.f, .5, .5}));
	DrawText(CTX_HOLDER.game_name.c_str(), 100, 100, 48, RAYWHITE);
	DrawText("Press Enter to start the game", 100, 200, 32, RAYWHITE);
	DrawText("Use buttons 1,2,3,4,5,6 to cast spells", 100, 250, 32, RAYWHITE);
	DrawText(credits, CTX_HOLDER.display_width -60 -right_align, CTX_HOLDER.display_height - 100, 28, RAYWHITE);
	EndDrawing();
}

Menu::Menu(): right_align(MeasureText(credits, 32))
{
	std::cout << "Menu::ctor" << std::endl;
}

// ----

void Transition::loop_run(float delta_t)
{
	ctx->draw();
	sleep += delta_t;
	/*if (sleep > 4.f) {
		NPatchInfo npinfo = { { 0.0f, 0.0f, 64.0f, 64.0f }, 6, 6, 6, 20, NPT_9PATCH };
		BeginDrawing();
		DrawTextureNPatch(baloon, npinfo, {0.f, 0.f, 120.f, 80.f}, {0.f, 0.f}, 0, WHITE);
		EndDrawing();
	}*/
	if (sleep > 8.f) {
		CTX_HOLDER.set_context(ctx);
	}
}

void Transition::start()
{
	ctx = dynamic_cast<Game*>(CTX_HOLDER.game);
}
