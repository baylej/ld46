#ifndef LD46_LEROY_H
#define LD46_LEROY_H

#include "../entities.h"
#include "foes.h"

class Leroy: public Movable, public Beatable, public Collidable {
public:
	const float aggro_range = 48.0f; // 2 tiles
	const float dmg_radius = 8.0f;

	Leroy();
	~Leroy();

	// Load path from map
	void initialise(const Map& map);

	// You can't stand waiting any more ... time to rush
	void start();

	// Draw Leroy's sprite at x,y
	void draw() const override;

	// Leroy's update function, called by Game.run_loop
	void update(float delta_t);

	// Check if should attack that flock (is in aggro range)
	void check_foes(Foe_flock* foe_flock);

protected:
	// override behaviours of trait Movable
	virtual bool hold_on() const override;
	virtual void arrived() override;
	virtual void on_death() override;
	virtual void on_dmg() override;

private:
	const Sprite sprite{"data/leroy.png"};
	const Clip yell{"data/leroy.wav"};
	const Clip die{"data/gitgud.wav"};
	const Clip hurt{"data/hurt.wav"};
	const Clip sword{"data/sword.wav"};

	std::vector<Vector2> path;
	int current_target = 0;

	Foe_flock* attacking = nullptr;
	bool rolling_back = false; // Once the flock is dead
	Vector2 attack_start{0}; // remember where attack started

	// Leroy will strike foes intersecting with this shape
	Convex_hull strike_shape;

	// update go to the next position from path, or end game
	void next_target();
	// Continue following path defined in map
	void follow_path();
	// Move back to attack_start
	void rollback();
	// Get the strike shape the current position and orientation
	Convex_hull get_strike_shape() const noexcept;
};

#endif //LD46_LEROY_H
