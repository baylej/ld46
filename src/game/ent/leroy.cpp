#include <iostream>

#include "leroy.h"

Leroy::Leroy(): Movable(3.f), Beatable(.3, 1.5), Collidable(8.f), strike_shape(4)
{
	std::cout << "Leroy ctor" << std::endl;
	strike_shape.points[0] = {-15,  5};
	strike_shape.points[1] = { 15,  5};
	strike_shape.points[2] = {  8, 15};
	strike_shape.points[3] = { -8, 15};
}

Leroy::~Leroy()
{
	std::cout << "Leroy dtor" << std::endl;
}

void Leroy::initialise(const Map &map)
{
	auto object = map.get_object("leroy", "path");
	assert(object != nullptr);
	assert(object->obj_type == OT_POLYLINE);

	tmx_shape* shape = object->content.shape;
	assert(shape != nullptr);
	assert(shape->points_len > 1);

	double off_x = object->x;
	double off_y = object->y;

	path.clear();
	path.reserve(shape->points_len);

	for (int it = 0; it < shape->points_len; it++) {
		Vector2 point {(float) (off_x + shape->points[it][0]), (float) (off_y + shape->points[it][1])};
		std::cout << "leroy path[" << it << "]=" << point.x << ", " << point.y << std::endl;
		path.push_back(point);
	}

	Vector2 spawn = path.at(0);
	x = spawn.x;
	y = spawn.y;

	current_target = 1;
}

void Leroy::start()
{
	follow_path();
	PlaySound(yell);
}

void Leroy::next_target()
{
	if (current_target < path.size() - 1) {
		current_target++;
		std::cout << "Leroy's new target: " << current_target << std::endl;
		target = path.at(current_target);
	}
		// Else: we're at the end of the path, no need to move anymore
	else {
		CTX_HOLDER.set_context(CTX_HOLDER.gameover);
	}
}

bool Leroy::hold_on() const
{
	if (attacking != nullptr) {
		return collide(Vector2{move_dir.x * speed, move_dir.y * speed}, attacking->get_colliding());
	}
	return false;
}

void Leroy::arrived()
{
	if (rolling_back) {
		rolling_back = false;
		follow_path();
	}
	else if (attacking != nullptr) {
		return;
	}
	else {
		next_target();
	}
}

void Leroy::update(float delta_t)
{
	elapsed_time(delta_t);
	if (dead()) {
		CTX_HOLDER.set_context(CTX_HOLDER.gameover);
	}

	move(delta_t);

	if (attacking != nullptr)
	{
		if (attacking->dead()) {
			rollback();
		}
		else {
			Foe* foe = attacking->nearest_alive_foe(*this);
			if (foe != nullptr) {
				target = *foe;
				Convex_hull shape = get_strike_shape();
				if (shape.intersects({foe->x, foe->y}, foe->dmg_radius)) {
					strike(*foe);
					PlaySound(sword);
				}
			}
		}
	}
}

void Leroy::check_foes(Foe_flock* foe_flock)
{
	if (rolling_back || attacking != nullptr || foe_flock->dead()) return;

	if (CheckCollisionCircleRec(*this, aggro_range, foe_flock->get_bbox())) {
		foe_flock->aggroed();
		attacking = foe_flock;
		attack_start = Vector2{x, y};
		std::cout << "Leroy is attacking a flock" << std::endl;
	}
}

void Leroy::follow_path()
{
	target = path.at(current_target);
	moving = true;
	std::cout << "Leroy going to " << current_target << std::endl;
}

void Leroy::rollback()
{
	target = attack_start;
	moving = true;
	attacking = nullptr;
	rolling_back = true;
	attack_start = Vector2{0};
	std::cout << "Leroy is rolling back" << std::endl;
}

void Leroy::draw() const
{
	int shift = 0;
	if (dead()) {
		shift = 2;
	}
	else if (moved) {
		shift = glm::floor(glm::fract((float)GetTime()) + .5);
	}
	DrawTexturePro(sprite, {shift*32.f, 0, 32, 32}, {x, y, 32, 32}, {16, 16}, orient * 180/PI, WHITE);

	if (!dead()) draw_health(x, y-15, health);

	if (CTX_HOLDER.dbg_overlay) {
		DrawCircleLines(x, y, aggro_range, YELLOW);

		Convex_hull shape = get_strike_shape();
		for (int j=0; j<shape.points.size()-1; j++) {
			glm::vec2& p1 = shape.points[j];
			glm::vec2& p2 = shape.points[j+1];
			DrawLineEx(Vector2{p1.x, p1.y}, Vector2{p2.x, p2.y}, 1.f, SKYBLUE);
		}
		glm::vec2& p1 = shape.points[0];
		glm::vec2& p2 = shape.points[shape.points.size()-1];
		DrawLineEx(Vector2{p1.x, p1.y}, Vector2{p2.x, p2.y}, 1.f, SKYBLUE);
	}
}

Convex_hull Leroy::get_strike_shape() const noexcept
{
	Convex_hull shape(strike_shape);
	shape.rotate(orient);
	shape.translate(glm::vec2(x, y) + glm::vec2(move_dir.x, move_dir.y));
	return shape;
}

void Leroy::on_death()
{
	PlaySound(die);
	orient = 0.f;
}

void Leroy::on_dmg()
{
	PlaySound(hurt);
}
