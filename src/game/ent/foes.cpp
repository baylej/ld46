#include "leroy.h"

const Convex_hull Foe::strike_shape{{{-10,5}, {10,5}, {8,15}, {-8,15}}};

void Foe::draw() const
{
	int shift = 0;
	if (dead()) {
		shift = 2;
	}
	else if (moved) {
		shift = glm::floor(glm::fract((float)GetTime()) + .5);
	}
	DrawTexturePro(rc, {shift*32.f, 0, 32, 32}, {x, y, 32, 32}, {16, 16}, orient * 180/PI, WHITE);

	if (!dead()) draw_health(x, y-15, health);

	if (CTX_HOLDER.dbg_overlay) {
		DrawCircleLines(x, y, dmg_radius, moving? RED: GREEN);

		Convex_hull shape = get_strike_shape();
		for (int j=0; j<shape.points.size()-1; j++) {
			glm::vec2& p1 = shape.points[j];
			glm::vec2& p2 = shape.points[j+1];
			DrawLineEx(Vector2{p1.x, p1.y}, Vector2{p2.x, p2.y}, 1.f, SKYBLUE);
		}
		glm::vec2& p1 = shape.points[0];
		glm::vec2& p2 = shape.points[shape.points.size()-1];
		DrawLineEx(Vector2{p1.x, p1.y}, Vector2{p2.x, p2.y}, 1.f, SKYBLUE);
	}
}

bool Foe::hold_on() const
{
	if (flock != nullptr) {
		return collide(Vector2{move_dir.x * speed, move_dir.y * speed}, flock->get_colliding());
	}
	return false;
}

void Foe::attack(Leroy* leroy)
{
	assert(leroy != nullptr);
	target = *leroy;
	Convex_hull shape = get_strike_shape();
	if (shape.intersects({leroy->x, leroy->y}, leroy->dmg_radius)) {
		strike(*leroy);
		PlaySound(foe_strike);
	}
	moving = true;

}

Convex_hull Foe::get_strike_shape() const noexcept
{
	Convex_hull shape(strike_shape);
	shape.rotate(orient);
	shape.translate(glm::vec2(x, y) + glm::vec2(move_dir.x, move_dir.y));
	return shape;
}

void Foe::on_death()
{
	PlaySound(foe_die);
}

void Foe_flock::draw() const
{
	for (const auto& foe: foes) {
		foe.draw();
	}
	if (CTX_HOLDER.dbg_overlay) {
		DrawRectangleLinesEx(bbox, 1, aggro? RED: GREEN);
	}
}

Foe* Foe_flock::nearest_alive_foe(const Vector2& pos)
{
	auto binop = [&pos](Foe* f1, Foe& f2) -> Foe* {
		if (f1 == nullptr && !f2.dead()) return &f2;
		if (f2.dead()) return f1;
		glm::vec2 a{pos.x, pos.y};
		glm::vec2 b{f1->x, f1->y};
		glm::vec2 c{f2.x, f2.y};
		return (glm::length(c-a) < glm::length(b-a))? &f2: f1;
	};
	return std::accumulate(foes.begin(), foes.end(), static_cast<Foe*>(nullptr), binop);
}

void Foe_flock::aggroed()
{
	aggro = true;
	for (auto&& foe: foes) foe.attack(attacking);
}

void Foe_flock::update(float delta_t)
{
	if (aggro) {
		for (auto&& foe: foes) {
			if (!foe.dead()) {
				foe.elapsed_time(delta_t);
				foe.attack(attacking);
				foe.move(delta_t);
			}
		}
	}
}

void Foe_flock::init()
{
	for (auto&& foe: foes) {
		foe.set_flock(this);
		colliding.emplace_back(&foe);
	}
	colliding.emplace_back(attacking);
}
