#ifndef LD46_PLAYER_H
#define LD46_PLAYER_H

#include "../entities.h"
#include "leroy.h"

class Player: public Movable {
public:
	Player(): Movable(2.0f) {};

	// Post construction
	void init(Leroy* leroy);

	// Draw the player's sprite at x,y
	void draw() const override;

	void draw_ui() const noexcept;

	// Update function, called by Game.run_loop
	void update(float delta_t);

protected:
	// override behaviours of trait Movable
	virtual bool hold_on() const override;
	virtual void arrived() override {}; // NOOP

private:
	const Sprite sprite{"data/healer.png"};
	const Sprite ui_sprite{"data/ui.png"};
	Leroy* leroy;

	float speel_1_cd = 0.f;
	float speel_2_cd = 0.f;
	float speel_3_cd = 0.f;
	float speel_4_cd = 0.f;
	float speel_5_cd = 0.f;
	float speel_6_cd = 0.f;
};

#endif //LD46_PLAYER_H
