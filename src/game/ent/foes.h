#ifndef LD46_FOES_H
#define LD46_FOES_H

#include <numeric>
#include <functional>

#include "../entities.h"

class Foe_flock;
class Leroy;

class Foe: public Movable, public Beatable, public Collidable {
public:
	const float dmg_radius = 8.0f;
	Foe(const Sprite& rc, const Clip& foe_strike, const Clip& foe_die, float x, float y):
	        Entity{x, y}, Movable(1.f), Collidable(8.f), Beatable(1.f, .1f, 1.f, 1.22f),
	        rc(rc), foe_strike(foe_strike), foe_die(foe_die) {}

	void draw() const override;
	bool can_collide() const noexcept override { return !dead(); }

	// Called by the flock on aggro
	void attack(Leroy* leroy);
	// Cast to bool:=> dead() used by Foe_flock::dead
	operator bool() const { return dead(); };
	void set_flock(Foe_flock* flock) noexcept { this->flock = flock; };

protected:
	// override behaviours of trait Movable
	virtual bool hold_on() const override;
	virtual void arrived() override { moving = false; };
	virtual void on_death() override;

private:
	const Sprite& rc;
	const Clip& foe_strike;
	const Clip& foe_die;
	Foe_flock* flock = nullptr;

	// Strike leroy if he's intersecting with this shape
	static const Convex_hull strike_shape;
	Convex_hull get_strike_shape() const noexcept;
};

// Aggregate of Foes
class Foe_flock {
public:
	explicit Foe_flock(Leroy* leroy): attacking(leroy) {}

	void add_foe(const Foe& to_add) { foes.push_back(to_add); }
	auto size() const noexcept { return foes.size(); }

	// Set bounding box for foes to roam and to call aggroed in case of intersection with leroy's aggro radius
	void set_bbox(Rectangle to_set) noexcept { bbox = to_set; }
	const Rectangle& get_bbox() const noexcept { return bbox; }

	void draw() const;

	void update(float delta_t);

	// Notify the flock it has been aggroed
	void aggroed();

	// true if all foes are dead
	bool dead() { return std::accumulate(foes.begin(), foes.end(), true, std::logical_and<bool>()); }

	// Called by leroy
	Foe* nearest_alive_foe(const Vector2& pos);

	std::vector<Foe>& get_foes() { return foes; }
	std::vector<Collidable*>& get_colliding() { return colliding; }

	void init();

private:
	std::vector<Foe> foes;
	Rectangle bbox{0};
	bool aggro = false;
	Leroy* attacking;
	std::vector<Collidable*> colliding;
};

#endif //LD46_FOES_H
