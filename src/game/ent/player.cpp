#include <iostream>

#include "player.h"

void Player::init(Leroy* leroy)
{
	this->leroy = leroy;

	x = leroy->x - 16.f;
	y = leroy->y - 16.f;

	moving = true;
}

void Player::draw() const
{
	DrawCircleLines(x, y, 8., RED);

	int shift = 0;
	if (moved) {
		shift = glm::floor(glm::fract((float)GetTime()) + .5);
	}
	DrawTexturePro(sprite, {shift*32.f, 0, 32, 32}, {x, y, 32, 32}, {16, 16}, orient * 180/PI, WHITE);
}

bool Player::hold_on() const
{
	static constexpr int max = 72*72;
	glm::vec2 tgt(leroy->x - x, leroy->y - y);
	float dst = tgt.x * tgt.x + tgt.y * tgt.y;
	return dst < max;
}

void Player::update(float delta_t)
{
	speel_1_cd -= delta_t;
	speel_2_cd -= delta_t;
	speel_3_cd -= delta_t;
	speel_4_cd -= delta_t;
	speel_5_cd -= delta_t;
	speel_6_cd -= delta_t;

	target = {leroy->x, leroy->y};
	move(delta_t);

	if (IsKeyPressed(KEY_ONE) && speel_1_cd <= 0.f)
	{
		//std::cout << "Heal1" << std::endl;
		leroy->heal(.01f);
		speel_1_cd = 3.f;
	}
	if (IsKeyPressed(KEY_TWO) && speel_2_cd <= 0.f)
	{
		//std::cout << "Heal2" << std::endl;
		leroy->heal(.05f);
		speel_2_cd = 5.f;
	}
	if (IsKeyPressed(KEY_THREE) && speel_3_cd <= 0.f)
	{
		//std::cout << "Heal3" << std::endl;
		leroy->heal(.1f);
		speel_3_cd = 10.f;
	}
	if (IsKeyPressed(KEY_FOUR) && speel_4_cd <= 0.f)
	{
		//std::cout << "Heal4" << std::endl;
		leroy->heal(.2f);
		speel_4_cd = 20.f;
	}
	if (IsKeyPressed(KEY_FIVE) && speel_5_cd <= 0.f)
	{
		//std::cout << "Heal5" << std::endl;
		leroy->heal(.5f);
		speel_5_cd = 30.f;
	}
	if (IsKeyPressed(KEY_SIX) && speel_6_cd <= 0.f)
	{
		//std::cout << "Heal6" << std::endl;
		leroy->heal(1.f);
		speel_6_cd = 300.f;
	}
}

void Player::draw_ui() const noexcept
{
	Vector2 pos{0};
	pos.x = CTX_HOLDER.display_width/2. - ui_sprite.width;
	pos.y = CTX_HOLDER.display_height - ui_sprite.height * 2;
	DrawTextureEx(ui_sprite, pos, 0.f, CTX_HOLDER.zoom, WHITE);

	if (speel_1_cd > 0.f) {
		DrawRectangle(120, 504, 2*39 * speel_1_cd/3., 2*39, Fade(GRAY, .5f));
	}
	if (speel_2_cd > 0.f) {
		DrawRectangle(120 + 96, 504, 2*39 * speel_2_cd/5., 2*39, Fade(GRAY, .5f));
	}
	if (speel_3_cd > 0.f) {
		DrawRectangle(120 + 96*2, 504, 2*39 * speel_3_cd/10., 2*39, Fade(GRAY, .5f));
	}
	if (speel_4_cd > 0.f) {
		DrawRectangle(120 + 96*3, 504, 2*39 * speel_4_cd/20., 2*39, Fade(GRAY, .5f));
	}
	if (speel_5_cd > 0.f) {
		DrawRectangle(120 + 96*4, 504, 2*39 * speel_5_cd/30., 2*39, Fade(GRAY, .5f));
	}
	if (speel_6_cd > 0.f) {
		DrawRectangle(120 + 96*5, 504, 2*39 * speel_6_cd/300., 2*39, Fade(GRAY, .5f));
	}
}
