#ifndef LD46_GAME_H
#define LD46_GAME_H

#include "../context.h"
#include "map.h"
#include "entities.h"
#include "ent/leroy.h"
#include "ent/foes.h"
#include "ent/player.h"

class Game: public Context {
public:
	Game();
	void init();
	void start() override;
	void loop_run(float delta_t) override;
	// Just the fraw part of the main loop, nothing gets updated
	void draw();

private:
	bool initialiased = false;
	Map map;
	Leroy leroy;
	Player player;
	Camera2D camera{0};
	const Sprite foe_rc{"data/foe.png"};
	const Clip foe_strike{"data/strike.wav"};
	const Clip foe_die{"data/die.wav"};
	std::vector<Foe_flock> foe_flocks;
};

#endif //LD46_GAME_H
