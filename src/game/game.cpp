#include <cassert>
#include <iostream>

#include "game.h"

Game::Game()
{
	std::cout << "Game::ctor" << std::endl;
	camera.zoom = CTX_HOLDER.zoom;
	camera.offset.x = CTX_HOLDER.display_width/2.;
	camera.offset.y = CTX_HOLDER.display_height/2.;
}

void Game::init()
{
	if (initialiased) return;

	assert(FileExists("data/map.tmx"));
	map.load("data/map.tmx");

	leroy.initialise(map);

	// All foes defined in the map
	auto res = map.get_layer<tmx_layer, L_GROUP>("foes");
	for (; res != nullptr; res = res->next) { // each flock of foes
		assert(res->type == L_OBJGR);
		Foe_flock& flock = foe_flocks.emplace_back(&leroy);
		for (auto foe_obj = res->content.objgr->head; foe_obj != nullptr; foe_obj = foe_obj->next) { // each foe in flock
			if (foe_obj->obj_type == OT_POINT)
				flock.add_foe(Foe(foe_rc, foe_strike, foe_die, foe_obj->x, foe_obj->y));
			else if (foe_obj->obj_type == OT_SQUARE)
				flock.set_bbox(Rectangle{(float)foe_obj->x, (float)foe_obj->y, (float)foe_obj->width, (float)foe_obj->height});
		}
		std::cout << "new flock of foes containing " << flock.size() << " foes" << std::endl;
	}
	std::cout << "Total: " << foe_flocks.size() << " flocks of foes" << std::endl;
	for (auto&& flock: foe_flocks) {
		flock.init();
	}

	player.init(&leroy);
	camera.target = {leroy.x, leroy.y+20};

	initialiased = true;
}

void Game::loop_run(float delta_t)
{
	// Update
	leroy.update(delta_t);
	for (auto&& foe_flock: foe_flocks) {
		leroy.check_foes(&foe_flock);
		foe_flock.update(delta_t);
	}
	player.update(delta_t);
	camera.target = {leroy.x, leroy.y+20};

	// Draw
	draw();
}

void Game::draw()
{
	BeginDrawing();
	BeginMode2D(camera);
	ClearBackground((BLACK));
	map.draw();
	for (const auto& foe_flock: foe_flocks) {
		foe_flock.draw();
	}
	leroy.draw();
	player.draw();
	EndMode2D();
	player.draw_ui();
	EndDrawing();
}

void Game::start()
{
	leroy.start();
}
