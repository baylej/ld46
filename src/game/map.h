#ifndef LD46_MAP_H
#define LD46_MAP_H

#include <memory>
#include <functional>
#include <optional>

#include "../context.h"
#include "tmx/tmx.h"

class Map {
public:
	Map() noexcept;

	// Load map and set rc_map (delete previous map is not null)
	void load(const std::string& path_to_map);
	// Draw map onto the current framebuffer
	void draw() const;

	// Get a layer by name (no free)
	template<typename T, tmx_layer_type ly_type>
	T* get_layer(const std::string& layer_name) const noexcept;

	// Get an object by name (no free)
	tmx_object* get_object(const std::string& group_name, const std::string& object_name) const noexcept;
private:
	std::unique_ptr<tmx_map, std::function<void(tmx_map*)>> rc_map;
};

#endif //LD46_MAP_H
