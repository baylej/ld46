#include <iostream>
#include <cassert>
#include <stdexcept>
#include <raylib.h>

#include "map.h"

void* raylib_tex_loader(const char* path)
{
	auto returnValue = new Texture2D;
	*returnValue = LoadTexture(path);
	//std::cout << "tex.w=" << returnValue->width << " tex.h=" << returnValue->height << std::endl;
	return returnValue;
}

void raylib_free_tex(void* ptr)
{
	auto tex = static_cast<Texture2D*>(ptr);
	UnloadTexture(*tex);
	delete tex;
}


Map::Map() noexcept
{
	// Set the callback globs in the main function
	tmx_img_load_func = raylib_tex_loader;
	tmx_img_free_func = raylib_free_tex;
}

void Map::load(const std::string& path_to_map)
{
	rc_map = std::unique_ptr<tmx_map, std::function<void(tmx_map*)>>(tmx_load(path_to_map.c_str()), tmx_map_free);
	if (rc_map == nullptr) {
		throw std::runtime_error(tmx_strerr());
	}
}

// map rendering functions
Color GetColour(unsigned int tmx_col)
{
	Color color = GetColor(tmx_col << 8);
	color.a = 255;
	return color;
}

void draw_image_layer(tmx_image* image) {
	auto texture = static_cast<Texture2D*>(image->resource_image);
	DrawTexture(*texture, 0, 0, WHITE);
}

void draw_polyline(double offset_x, double offset_y, double** points, int points_count, Color color) {
	int i;
	for (i=1; i<points_count; i++) {
		DrawLine(offset_x + points[i-1][0],
		         offset_y + points[i-1][1],
		         offset_x + points[i][0],
		         offset_y + points[i][1], color);
	}
}

void draw_polygon(double offset_x, double offset_y, double **points, int points_count, Color color) {
	draw_polyline(offset_x, offset_y, points, points_count, color);
	if (points_count > 2) {
		DrawLine(offset_x + points[0][0],
		         offset_y + points[0][1],
		         offset_x + points[points_count-1][0],
		         offset_y + points[points_count-1][1], color);
	}
}

void draw_objects(tmx_object_group* objgr) {
	tmx_object* head = objgr->head;
	Color color = GetColour(objgr->color);

	while (head) {
		if (head->visible) {
			if (head->obj_type == OT_SQUARE) {
				DrawRectangleLines(head->x, head->y, head->width, head->height, color);
			}
			else if (head->obj_type == OT_POLYGON) {
				draw_polygon(head->x, head->y, head->content.shape->points, head->content.shape->points_len, color);
			}
			else if (head->obj_type == OT_POLYLINE) {
				draw_polyline(head->x, head->y, head->content.shape->points, head->content.shape->points_len, color);
			}
			else if (head->obj_type == OT_ELLIPSE) {
				DrawEllipseLines(head->x + head->width/2.0, head->y + head->height/2.0, head->width/2.0, head->height/2.0, color);
			}
		}
		head = head->next;
	}
}

void draw_tile(void* image, unsigned int sx, unsigned int sy, unsigned int sw, unsigned int sh,
               unsigned int dx, unsigned int dy, float opacity, unsigned int flags) {
	auto texture = static_cast<Texture2D*>(image);
	unsigned char op = 0xFF * opacity;
	DrawTextureRec(*texture,
	               {(float)sx, (float)sy, (float)sw, (float)sh},
	               {(float)dx, (float)dy},
	               {op, op, op, op});
}

void draw_layer(tmx_map* map, tmx_layer* layer) {
	unsigned long i, j;
	unsigned int gid, x, y, w, h, flags;
	float op;
	tmx_tileset* ts;
	tmx_image* im;
	void* image;
	op = layer->opacity;
	for (i = 0; i < map->height; i++) {
		for (j = 0; j < map->width; j++) {
			gid = (layer->content.gids[(i * map->width) + j]) & TMX_FLIP_BITS_REMOVAL;
			if (map->tiles[gid] != NULL) {
				ts = map->tiles[gid]->tileset;
				im = map->tiles[gid]->image;
				x = map->tiles[gid]->ul_x;
				y = map->tiles[gid]->ul_y;
				w = ts->tile_width;
				h = ts->tile_height;
				if (im) {
					image = im->resource_image;
				}
				else {
					image = ts->image->resource_image;
				}
				flags = (layer->content.gids[(i * map->width) + j]) & ~TMX_FLIP_BITS_REMOVAL;
				draw_tile(image, x, y, w, h, j * ts->tile_width, i * ts->tile_height, op, flags);
			}
		}
	}
}

void Map::draw() const
{
	assert(rc_map != nullptr); // Call load at least once

	ClearBackground(GetColour(rc_map->backgroundcolor));
	for (auto it = rc_map->ly_head; it != nullptr; it = it->next) {
		if (it->visible) {
			if (it->type == L_IMAGE) {
				draw_image_layer(it->content.image);
			}
			else if (it->type == L_LAYER) {
				draw_layer(rc_map.get(), it);
			}
			else if (CTX_HOLDER.dbg_overlay && it->type == L_OBJGR) {
				draw_objects(it->content.objgr);
			}
		}
	}
}

tmx_object* Map::get_object(const std::string& group_name, const std::string& object_name) const noexcept
{
	auto objgr = get_layer<tmx_object_group, L_OBJGR>(group_name);
	for (auto it = objgr->head; it != nullptr; it = it->next) {
		if (object_name == it->name) {
			return it;
		}
	}
	return nullptr;
}

template<typename T, tmx_layer_type ly_type>
T* Map::get_layer(const std::string& layer_name) const noexcept
{
	for (auto it = rc_map->ly_head; it != nullptr; it = it->next) {
		if (it->type == ly_type && layer_name == it->name)
		{
			return reinterpret_cast<T*>(it->content.gids); // Legal for a C union (all members are pointers)
		}
	}
	return nullptr;
}

template
tmx_layer* Map::get_layer<tmx_layer, L_GROUP>(const std::string& layer_name) const noexcept;

template
tmx_image* Map::get_layer<tmx_image , L_IMAGE>(const std::string& layer_name) const noexcept;
