#include "entities.h"

void Movable::move(float delta_t) {
	moved = false;
	if (!moving) return;

	// Current position and target destination
	glm::vec2 pos{x, y};
	glm::vec2 dest{target.x, target.y};
	// Direction unit vector
	glm::vec2 move = glm::normalize(dest - pos);
	move_dir = Vector2{move.x, move.y};

	// Direction angle with vert vec
	orient = glm::acos(glm::dot(glm::vec2(0., 1.), move)) * -m_sign(move.x);

	// Cancel move
	if (hold_on()) return;

	// New x,y position
	glm::vec2 new_pos = pos + move * speed * 30.f * delta_t;

	// Are we there yet?
	glm::vec2 next_move = glm::normalize(dest - new_pos);
	if (glm::dot(move, next_move) < .99) {
		arrived();
		return;
	}

	x = new_pos.x;
	y = new_pos.y;

	moved = true;
}

bool Collidable::collide(const Vector2& move, const std::vector<Collidable*>& others) const noexcept
{
	glm::vec2 pos{x, y};
	pos += glm::vec2{move.x, move.y};

	for (const auto other: others) {
		if (this == other) continue;
		if (!other->can_collide()) continue;
		glm::vec2 o_pos{other->x, other->y};
		glm::vec2 vAB = o_pos - pos;
		float sqDist = (vAB.x * vAB.x + vAB.y * vAB.y);
		float sqMax = (collision_radius + other->collision_radius); sqMax = sqMax*sqMax;
		if (sqDist <= sqMax)
			return true;
	}
	return false;
}
