#ifndef LD46_ENTITIES_H
#define LD46_ENTITIES_H

#include "../context.h"
#include "utils.h"
#include "map.h"

// Base type for things to display
class Entity: public Vector2 {
public:
	Entity(): Vector2{0} {};
	Entity(float x, float y): Vector2{x, y} {};
	virtual void draw() const = 0;
};

// Moves towards a target unless has to stop
class Movable: public virtual Entity {
public:
	Movable(float speed): speed(speed) {};
	void set_target(const Vector2& to_set) noexcept { target = to_set; }
	// Sets move_dir, call hold_on and proceed to update this position
	virtual void move(float delta_t);
protected:
	Vector2 move_dir{0}; // unit vector, set by move
	float speed; // in pixels, used by move
	float orient; // angle in rad with vertical upward vector, set by move
	bool moving = false; // if false, move returns
	bool moved = false; // true if move() updated this entity's position
	Vector2 target{0}; // destination, used by move

	// Condition to cancel a move
	virtual bool hold_on() const { return false; };
	// Callback when we're at the target
	virtual void arrived() = 0;
};

// Can collide, accumulate reaction to collisions
class Collidable: public virtual Entity {
public:
	// Radius for collision
	const float collision_radius;

	Collidable(float radius): collision_radius(radius) {}

	// return true of the given move vector may move us into one of the supplied others
	bool collide(const Vector2& move, const std::vector<Collidable*>& others) const noexcept;

	// Return false if this cannot collide (not solid at the moment)
	virtual bool can_collide() const noexcept { return true; }

	// return true actively colliding with other
	bool collide(Collidable& other) const noexcept { return CheckCollisionCircles(*this, collision_radius, other, other.collision_radius); }
};

class Beatable {
public:
	Beatable(float armor = 1.f, float base_dmg = .1f, float health = 1.f, float cooldown_value = 1.f):
	        armor(armor), base_dmg(base_dmg), health(health), cooldown_value(cooldown_value) {}

	// Hit by other (no cooldown)
	void hit(const Beatable& other) noexcept { set_health(health - other.base_dmg * armor); if (dead()) on_death(); else on_dmg(); }

	// if cooldoown <=0, Strike other (calls other.hit(this)) then sets the cooldown
	void strike(Beatable& other) noexcept { if (cooldown <= 0) { other.hit(*this); cooldown = cooldown_value; } }

	// return true if health is lower or equal to zero
	bool dead() const noexcept { return health <= 0.; }

	// decrease the cooldown of the given value
	void elapsed_time(float delta_t) { cooldown-= delta_t; }

	void heal(float amount) { set_health(health + amount); }

protected:
	float health; // always 1 (except bosses)

	// Clamped health setter
	void set_health(float value) { health = glm::clamp(value, 0.f, 1.f); }

	// Callback on events
	virtual void on_dmg() {};
	virtual void on_death() {};

private:
	float armor; // damage reduction, the lower the better

	float base_dmg; // dmg value, the higher the better
	const float cooldown_value; // cooldown in seconds
	float cooldown = 0.f; // to wait between two strikes
};

#endif //LD46_ENTITIES_H
