#include <iostream>

#include "utils.h"

Convex_hull::Convex_hull(int len): points(len) {}

Convex_hull::Convex_hull(const std::vector<glm::vec2>& points): points(points) {}

Convex_hull::Convex_hull(const Convex_hull& toCopy): Convex_hull(toCopy.points) {}

Convex_hull::Convex_hull(Convex_hull&& to_move): points(std::move(to_move.points)) {}

void Convex_hull::translate(glm::vec2 tr)
{
	for (int i=0; i<points.size(); i++) {
		points[i] += tr;
	}
}

void Convex_hull::rotate(float orient)
{
	if (orient != 0) {
		glm::mat2 rotm = get_rot_mat2(orient);

		for (int i=0; i<points.size(); i++) {
			points[i] = rotm * points[i];
		}
	}
}

bool Convex_hull::intersects(Convex_hull &other) const
{
	bool isIn;
	float sign;
	int i, j;
	glm::vec2 pA, pB, pC;
	glm::vec2 vAB, vAC;

	// for each points of the other hull
	for (i=0; i<other.points.size(); i++) {
		// test if the point is at the right (-1) or at the left (+1)
		// of a segment made from two points of this hull
		pA = points[points.size()-1];
		pB = points[0];
		pC = other.points[i];

		vAB = pB - pA;
		vAC = pC - pA;

		sign = m_sign(vAB.x * vAC.y - vAB.y * vAC.x);
		isIn = true;

		// for each segment of this hull FIXME: One segment is missing!!!!!!
		for (j=0; j<points.size()-1; j++) {
			// if the point is not at the same side of each segment
			pA = points[j];
			pB = points[j+1];

			vAB = pB - pA;
			vAC = pC - pA;

			if (sign != m_sign(vAB.x * vAC.y - vAB.y * vAC.x)) {
				isIn = false; // it is not inside this hull
				break;
			}
		}
		if (isIn) {
			return true;
		}
	}
	return false;
}

bool Convex_hull::intersects(glm::vec2 pC, float radius) const
{
	float radius_square = radius*radius;
	float sign = 0.f;
	bool is_in = true;

	auto segment_circle_intersect = [&](const glm::vec2& pA, const glm::vec2& pB) -> bool {
		glm::vec2 vAC = pC - pA;
		glm::vec2 vAB = pB - pA;

		float sqlAC = (vAC.x * vAC.x + vAC.y * vAC.y);

		// point of hull is in circle
		if (sqlAC < radius_square) return true;

		float sqlAB = (vAB.x * vAB.x + vAB.y * vAB.y);

		// Check that the circle is not completely included within the hull
		if (sign == 0.f) {
			sign = m_sign(vAB.x * vAC.y - vAB.y * vAC.x);
		}
		else if (is_in && sign != m_sign(vAB.x * vAC.y - vAB.y * vAC.x)) {
			is_in = false;
		}

		// Does not intersect with segment
		if (sqlAC > sqlAB) return false;

		glm::vec2 vCB = pB - pC;
		float sqlCB = (vCB.x * vCB.x + vCB.y * vCB.y);

		// Does not intersect with segment
		if (sqlCB > sqlAB) return false;

		// at this step, projection pX of pC onto (AB) is within [AB]
		float ABdotAC = glm::dot(vAB, vAC); // cos(XAC) = cos(BAC)
		float sqlAX = (ABdotAC*ABdotAC)/sqlAB; // composition of dot products squared
		float sqlCX = sqlAC - sqlAX; // AXC = 90°

		// Segment and circle intersect
		return (sqlCX < radius_square);
	};

	// for each segment of this hull
	for (int j=0; j<points.size()-1; j++) {
		glm::vec2 pA = points[j];
		glm::vec2 pB = points[j+1];
		if (segment_circle_intersect(pA, pB)) return true;
	}
	glm::vec2 pA = points[points.size()-1]; // The order matter for the is_in mechanism
	glm::vec2 pB = points[0];
	if (segment_circle_intersect(pA, pB)) return true;

	return is_in;
}

// ----

void draw_health(float x, float y, float health)
{
	DrawLineEx(Vector2{x-10, y}, Vector2{x+10, y}, 2., RED);
	if (health > 0.)
		DrawLineEx(Vector2{x-10, y}, Vector2{x-10 + health*20, y}, 2., GREEN);
}
