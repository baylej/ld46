#ifndef LD46_UTILS_H
#define LD46_UTILS_H

//    MATHS

#include <vector>
#include <glm/glm.hpp>

#define m_sign(x) ((x) >= 0 ? 1. : -1.)

inline glm::mat2 get_rot_mat2(float orient)
{
	float c = glm::cos(orient);
	float s = glm::sin(orient);
	return glm::mat2(c, s, -s, c);
}

// Used for precise collisions
class Convex_hull {
public:
	std::vector<glm::vec2> points;

	Convex_hull(const std::vector<glm::vec2>& points); // Copy points
	Convex_hull(int len);
	Convex_hull(const Convex_hull& to_copy); // Copy points
	Convex_hull(Convex_hull&& to_move); // Move points

	void translate(glm::vec2 tr);
	void rotate(float orient); /// Counter-clock-wise rotation in radians

	bool intersects(Convex_hull &other) const;
	bool intersects(glm::vec2 center, float radius) const;
};

//    RAYLIB

#include <string>
#include <raylib.h>

// Texture2D with unloading dtor
class Sprite: public Texture2D {
public:
	explicit Sprite(const std::string& path): Texture2D(LoadTexture(path.c_str())) {}
	~Sprite() { UnloadTexture(*this); }
};

// Sound with unloading dtor
class Clip: public Sound {
public:
	explicit Clip(const std::string& path): Sound(LoadSound(path.c_str())) {}
	~Clip() { UnloadSound(*this); }
};

void draw_health(float x, float y, float health);

#endif //LD46_UTILS_H
