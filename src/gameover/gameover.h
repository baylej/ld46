#ifndef LD46_GAMEOVER_H
#define LD46_GAMEOVER_H

#include "../context.h"

class Gameover: public Context {
public:
	void loop_run(float delta_t) override;
};

#endif //LD46_GAMEOVER_H
