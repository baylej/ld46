#include <raylib.h>

#include "gameover.h"

void Gameover::loop_run(float delta_t)
{
	if (IsKeyPressed(KEY_ENTER)) {
		CTX_HOLDER.set_context(CTX_HOLDER.menu);
	}

	// draw menu
	BeginDrawing();
	ClearBackground(BLACK);
	DrawText("Thank you for playing", 100, 200, 32, RAYWHITE);
	DrawText("Press Enter to return to the menu", 100, 250, 32, RAYWHITE);
	EndDrawing();
}
