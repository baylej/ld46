#ifndef LD46_CONTEXT_H
#define LD46_CONTEXT_H

#include <string>

class Context {
public:
	// run by the main loop, delta_t is the elapsed time since last call.
	// Does:
	//  - Input
	//  - Update
	//  - Draw
	//  - Set context
	virtual void loop_run(float delta_t) = 0;
	// Called when switching to this context
	virtual void start() {};
};

// Holds the running context, set/get the running context
class Context_holder {
public:
	Context_holder() = default;
	Context_holder(const Context_holder&) = delete;
	Context_holder(Context_holder&&) = delete;
	Context_holder& operator =(const Context_holder&) = delete;
	Context_holder& operator =(Context_holder&&) = delete;

	// Context manipulation
	void set_context(Context* ctx) noexcept { ctx->start(); current = ctx; }
	Context* get_context() const noexcept { return current; }

	// Global Contexts (set in main function)
	Context* menu = nullptr;
	Context* game = nullptr;
	Context* gameover = nullptr;

	// Configuration (defaults)
	int fps = 30;
	int display_width = 800;
	int display_height = 600;
	float zoom = 2.f;
	const std::string game_name = "That guy...";
	const std::string data_dir = "data";
	bool fps_overlay = false; // greatly impacts performances (why?)
	bool dbg_overlay = false;

private:
	Context* current = nullptr;
};
extern Context_holder CTX_HOLDER;

#endif //LD46_CONTEXT_H
